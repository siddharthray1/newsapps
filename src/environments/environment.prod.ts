export const environment = {
	production: true
};

export const BASE_URL =
	'https://newsapi.org/v2/top-headlines?' +
	'sources=bbc-news&' +
	'apiKey=efe98f851b4c45d3bb1b51dfefb70369';
