import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
	{ path: '', redirectTo: 'home', pathMatch: 'full' },
	{
		path: 'home',
		loadChildren: () =>
			import('./news-page/news-page.module').then(m => m.NewsPagePageModule)
	},
	{
		path: 'selected-news/:id',
		loadChildren: () =>
			import('./selected-news/selected-news.module').then(
				m => m.SelectedNewsPageModule
			)
	}
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
	],
	exports: [RouterModule]
})
export class AppRoutingModule {}
