import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelectedNewsPage } from './selected-news.page';

describe('SelectedNewsPage', () => {
  let component: SelectedNewsPage;
  let fixture: ComponentFixture<SelectedNewsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectedNewsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelectedNewsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
