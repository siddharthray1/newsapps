import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
	selector: 'app-selected-news',
	templateUrl: './selected-news.page.html',
	styleUrls: ['./selected-news.page.scss']
})
export class SelectedNewsPage implements OnInit {
	errorMessage: any;
	posts: any;
	url: string;
	json: any;
	source: any;
	imgData: any;
	title: string;
	author: string;
	timeStamp: any;
	description: any;
	latestNews = JSON.parse(localStorage.getItem('latest-news')) || null;
	constructor(private route: ActivatedRoute, private iab: InAppBrowser) {}

	ngOnInit() {
		if (localStorage.getItem('latest-news')) {
			console.log('latest-news', localStorage.getItem('latest-news'));
			console.log('id from url', this.route.snapshot.params.id);
			const selectedId = this.route.snapshot.params.id;
			// debugger;
			this.source = this.latestNews[selectedId].source.name;
			this.description = this.latestNews[selectedId].description;
			this.imgData = this.latestNews[selectedId].urlToImage;
			this.author = this.latestNews[selectedId].author;
			this.timeStamp = this.latestNews[selectedId].publishedAt;
			this.title = this.latestNews[selectedId].title;
			console.log({
				source: this.source,
				description: this.description,
				imgData: this.imgData,
				author: this.author,
				timeStamp: this.timeStamp,
				title: this.title
			});
		}
	}
	openNewsPage() {
		const selectedId = this.route.snapshot.params.id;
		const selectedNews = this.latestNews[selectedId].url;
		const browser = this.iab.create(selectedNews);
	}
}
