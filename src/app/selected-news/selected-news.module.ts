import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectedNewsPageRoutingModule } from './selected-news-routing.module';

import { SelectedNewsPage } from './selected-news.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectedNewsPageRoutingModule
  ],
  declarations: [SelectedNewsPage]
})
export class SelectedNewsPageModule {}
