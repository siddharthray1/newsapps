import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectedNewsPage } from './selected-news.page';

const routes: Routes = [
  {
    path: '',
    component: SelectedNewsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectedNewsPageRoutingModule {}
