import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment, BASE_URL } from '../environments/environment';
import { News } from './model/news.model';

@Injectable({
	providedIn: 'root'
})
export class NewsService {
	baseUrl = BASE_URL;
	constructor(private http: HttpClient) {}

	getNews() {
		return this.http.get<News>(this.baseUrl);
	}
}
