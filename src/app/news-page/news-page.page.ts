import { Component, OnInit } from '@angular/core';
import { NewsService } from '../news.service';
import { Network } from '@ionic-native/network/ngx';
import { Toast } from '@ionic-native/toast/ngx';
import { Platform } from '@ionic/angular';
import { switchMap, debounceTime, catchError } from 'rxjs/operators';

@Component({
	selector: 'app-news-page',
	templateUrl: './news-page.page.html',
	styleUrls: ['./news-page.page.scss']
})
export class NewsPagePage implements OnInit {
	errorMessage: any;
	news: any;

	constructor(
		private newsService: NewsService,
		private network: Network,
		private toast: Toast,
		private platform: Platform
	) {}

	ngOnInit() {
		// watch network for a disconnection
		if (this.platform.is('android') || this.platform.is('ios')) {
			let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
				console.log('network was disconnected');
				this.toast.show(`Internet not connected`, '5000', 'center');
			});

			// watch network for a connection
			let connectSubscription = this.network.onConnect().subscribe(() => {
				console.log('network connected!');
				this.toast.show(`Internet is connected`, '5000', 'center');
			});
		}

		if (!localStorage.getItem('latest-news')) {
			this.getNews();
		} else {
			this.news = JSON.parse(localStorage.getItem('latest-news'));
		}
	}

	getNews() {
		return this.newsService.getNews().subscribe(
			news => {
				console.log(news);
				this.news = news.articles;
				localStorage.setItem('latest-news', JSON.stringify(this.news));
				// console.log(this.news.length);
			},
			error => {
				if (!localStorage.getItem('latest-news')) {
					this.news = null;
				} else {
					this.news = JSON.parse(localStorage.getItem('latest-news'));
					this.errorMessage = <any>error;
				}
			}
		);
	}
}
